<?php

/** 
 * In this file we collect a couple of strings that are to be translated but are
 * not named directly in the code. These are mainly the functions of committee
 * members. The code itself in this file isn't used but it is scanned by POEdit.
 */

__('Voorzitter');
__('Vice-voorzitter');
__('Secretaris');
__('Penningmeester');
__('Algemeen lid');
__('Fotograaf');
__('Commissaris Intern');
__('Commissaris Extern');
__('Ouderejaars lid');
__('Commissaris Zorgtoeslag');
__('Commissaris Kusje');
__('Commissaris Verbanddoos');
__('Huisarts');
__('Huisarts-assistent');
__('Commissaris Heethoofd');
__('Promohomo');
