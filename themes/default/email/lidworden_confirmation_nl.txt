Hallo $first_name,

Je aanmelding is bijna voltooid. Om je e-mail te bevestigen hoef je alleen de volgende link nog even te volgen:

https://www.svcover.nl/lidworden.php?confirmation_code=$confirmation_code

Na deze stap krijg je je wachtwoord toegestuurd en kan je eten en drinken in de Coverkamer halen.

Sommige functies, zoals de fotoalbums en lidgegevens die alleen toegankelijk zijn voor leden, zijn pas beschikbaar nadat de secretaris je aanvraag heeft verwerkt. Maar meestal duurt dat niet zo lang.

Groeten,

De AC/DCee